SHELL:=/bin/bash

include .env
export $(shell sed 's/=.*//' .env)

DOMAIN:=example.com

cluster-delete:
	k3d cluster delete argo

cluster-create:
	k3d cluster create argo \
		--servers 3 \
		-p $${HTTP_PORT:-80}:80@loadbalancer \
		-p $${HTTPS_PORT:-443}:443@loadbalancer \
		$${REMOTE_HOST:+--k3s-arg "--tls-san=$$REMOTE_HOST@server:*"} \
		--k3s-arg '--disable=traefik@server:*'

cluster-recreate: cluster-delete cluster-create

cluster-init:
	argocd-autopilot repo bootstrap --provider gitlab --recover

argocd-login:
	$(eval password := $(shell kubectl get secret/argocd-initial-admin-secret -n argocd -o template --template='{{.data.password}}' | base64 -d))
	argocd login argocd.$(DOMAIN)$${HTTPS_PORT:+:$$HTTPS_PORT} --insecure --username admin --password "$(password)"
	@echo -e "\n\e[0;2m***\e[0m You can longin into \e[34;4mhttp://argocd.$(DOMAIN)$${HTTP_PORT:+:$$HTTP_PORT}/\e[0m by admin's password \e[33m$(password)\e[0m\n"

inspect-app-argocd-vault-plugin-test:
	argocd app get argocd/argocd-vault-plugin-test

hard-refresh-adpp-argocd-vault-plugin-test:
	argocd app get argocd/argocd-vault-plugin-test --hard-refresh


.PHONY: .vault-verify-address
.vault-verify-address:
	@VAULT_ADDR="$$(awk '/^VAULT_ADDR=/{print substr($$0,12)}' .env)"; \
	VAULT_URL="http://vault.$(DOMAIN)$${HTTP_PORT:+:$$HTTP_PORT}"; \
	[ "$$VAULT_ADDR" == "$$VAULT_URL" ] || sed -i "s#^\(VAULT_ADDR=\).*\$$#\1$$VAULT_URL#" .env

.PHONY: .vault-init
.vault-verify-token: .vault-verify-address
	$(eval export VAULT_ADDR = $(shell awk '/^VAULT_ADDR=/{print substr($$0,12)}' .env))
	set -o pipefail; vault status | awk '$$1=="Initialized"{r=$$2=="true"}END{exit !r}' || ([ $$? -eq 1 ] && vault operator init | tee .secrets)
	set -o pipefail; vault status | awk '$$1=="Sealed"{r=$$2=="false"}END{exit !r}' || awk '/^Unseal Key [1-3]: /{system("set -x; vault operator unseal "$$NF)}' .secrets

	@VAULT_TOKEN="$$(awk '/^VAULT_TOKEN=/{print substr($$0,13)}' .env)"; \
	VAULT_ROOT_TOKEN="$$(awk '/^Initial Root Token: /{print $$NF}' .secrets)"; \
	[ "$$VAULT_TOKEN" == "$$VAULT_ROOT_TOKEN" ] || sed -i "s#^\(VAULT_TOKEN=\).*\$$#\1$$VAULT_ROOT_TOKEN#" .env	

vault-init: .vault-verify-token
	$(eval export VAULT_TOKEN = $(shell awk '/^VAULT_TOKEN=/{print substr($$0,13)}' .env))
	terraform -chdir=apps/vault/tf init
	terraform -chdir=apps/vault/tf plan
	terraform -chdir=apps/vault/tf apply -auto-approve
	@echo -e "\n\e[0;2m***\e[0m You can longin into \e[34;4mhttp://vault.$(DOMAIN)$${HTTP_PORT:+:$$HTTP_PORT}/\e[0m by token \e[33m$(VAULT_TOKEN)\e[0m\n"

check-argocd-vault-plugin-result:
	@kubectl get secret/example-database -n default -o json | jq -r '.data|to_entries[]|.key+": "+.value' | awk '{("echo "$$2" | base64 -d")|getline $$2; print}'
