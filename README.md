# ArgoCD Bootstrap Example

## Prerequisites
* **environment**
  ```bash
  # Optional, if you cannot use default HTTP port (80).
  #HTTP_PORT=
  # Optional, if you cannot use default HTTPS port (443).
  #HTTPS_PORT=
  ```
* **access to cluster**
  ```bash
  # for example
	k3d cluster create argo \
		--servers 3 \
		-p $${HTTP_PORT:-80}:80@loadbalancer \
		-p $${HTTPS_PORT:-443}:443@loadbalancer \
		$${REMOTE_HOST:+--k3s-arg "--tls-san=$$REMOTE_HOST@server:*"} \
		--k3s-arg '--disable=traefik@server:*'
  # or
  make cluster-create
  ```
  _Options Notes:_
  * `--k3s-arg '--disable=traefik@server:*'` disables default ingress
    because we deploy it by argocd.
  * `--k3s-arg "--tls-san=$$REMOTE_HOST@server:*"` is necessary if you
    want use exernal Docker
  * `-p $${HTTP_PORT:-80}:80@loadbalancer`

  > **Warning**: we deploy ArgoCD in high avaiable mode, so it needs
  > more resources. If you are using Docker Desktop, please increase
  > memory and CPU cores for testing this deployment.
  > Another option is to use external Docker, defiend by 
  > `export DOCKER_HOST=ssh://some-remote-host`, where are
  > at least 16GB available.

* **git repository**
  ```bash
  # repo should be empty
  export GIT_REPO=https://gitlab.com/owner/name
  # token to repo with privileges for api and rew/write repo
  export GIT_TOKEN=ghp_PcZ...IP0
  ```
  _Notes:_ You can use any git provider. Access token nedds privileges
  not only to `read_repo`, but also `write_repo` and `api_read`. So
  you need fork this repo, for test this example.

* **cli tools**\
  `git`, `docker`, `k3d`, `vault`, `argocd`, `argocd-autopilot`,
  `kubectl`, `terraform`, `jq`, `base64`, `bash`, `awk`, `tee`, `ssh`

* **setup domains**\
  into `/etc/hosts` add entry:
  ```
  127.0.0.1  argocd.example.com vault.example.com traefik.example.com
  ```
  This avoids to set DNS records. If you want use docker on external
  server, replace `127.0.0.1` with its IP.

  **_Note:_** If you want to use other domain than `example.com`, find
  and replace all ocurences in files.

## Install

```bash
$ argocd-autopilot repo bootstrap --provider gitlab --recover
...
INFO argocd initialized. password: pfrDVRJZtHYZKzBv 
INFO run:

    kubectl port-forward -n argocd svc/argocd-server 8080:80
```

